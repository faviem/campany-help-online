import {Role} from "./role";
import {Departement} from "../back-office/departement";

export class Utilisateur {
    id: number;
    username: string;
    name: string;
    email: string;
    roles: Role[];
    password: string;
    departement:	Departement;
    groupe: Role;
    rolesName: string[];
}
