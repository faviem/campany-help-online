import {Categorie} from "./categorie";
import {Media} from "./media";

export class Question {
  categories: Categorie[];
  email: string;
  id: number;
  imageEnAvant: Media;
  libelle: string
  nombreAffectation: number;
  nombreReponse: number;

}
