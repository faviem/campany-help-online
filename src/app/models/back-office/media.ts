export class Media {
  fileName:	string;
  id:	number;
  mimeType:	string;
  urifile:	string;
  originalName: string;
}
