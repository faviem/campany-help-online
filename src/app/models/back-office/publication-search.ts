export class PublicationSearch {
  categoriesIds: number[];
  contenu: string;
  endDate: string;
  motClesIds: number[];
  startDate: string;
  status: string;
  titre: string;
}
