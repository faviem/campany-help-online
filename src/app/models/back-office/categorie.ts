import {Media} from "./media";

export class Categorie {
  id: number;
  libelle: string;
  code: string;
  description: string;
  experiencePartage: boolean;
  publier: boolean;
  imageAlaUne: Media;
}
