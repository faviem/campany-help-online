import {Media} from "./media";
import {MotCle} from "./mot-cle";
import {Categorie} from "./categorie";

export class Publication {
  categories: Categorie[];
  contenu: string
  dateAnnulation: string;
  datePublication: string;
  id: number;
  imageEnAvant: Media;
  motCles: MotCle[];
  ressources: Media[];
  slug: string
  status: string;
  titre: string;
}
