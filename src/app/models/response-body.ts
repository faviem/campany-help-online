export class ResponseBody {
  success: boolean;
  message: string;
  object: any;
  statusCode: string;
}
