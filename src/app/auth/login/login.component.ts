import { Component, OnInit } from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";
import {TokenStorage} from "../../utilitaires/token-storage";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isErrorAuthentication = false;
  isErrorMessage = '';

  validateForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private tokenStorage: TokenStorage, ) { }

  ngOnInit() {

    this.validateForm = this.fb.group({
      usernameOrEmail: [null, [Validators.required]
      ],
      password: [null, [Validators.required]],

      remember: [false]
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.valid === true) {
      const formData = this.validateForm.value;
      console.log(formData);
      this.authService.login(formData).subscribe(
        (data: any) => {
          if (data && data.accessToken) {
            this.tokenStorage.saveToken(data.accessToken);
            this.router.navigate(['/back-office']);
          } else {
            this.isErrorAuthentication = true;
            this.isErrorMessage = 'Identifiants erronés !';
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error?error.status : 'erreur');
        });
    } else {
      this.isErrorAuthentication = true;
      this.isErrorMessage = 'Veuillez entrer correctement vos identifiants.';
    }

  }

}
