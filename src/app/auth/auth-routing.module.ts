import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {ReinitialyzeAccountComponent} from "./reinitialyze-account/reinitialyze-account.component";
import {AuthComponent} from "./auth.component";
import {SingupComponent} from "./singup/singup.component";

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'reinitialyze',
        component: ReinitialyzeAccountComponent,
      },
      {
        path: 'inscription',
        component: SingupComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
