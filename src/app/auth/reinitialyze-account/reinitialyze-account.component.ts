import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenStorage} from "../../utilitaires/token-storage";
import {UtilisateurService} from "../../services/back-office/utilisateur.service";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-reinitialyze-account',
  templateUrl: './reinitialyze-account.component.html',
  styleUrls: ['./reinitialyze-account.component.css']
})
export class ReinitialyzeAccountComponent implements OnInit {


  isErrorAuthentication = false;
  isErrorMessage = '';

  validateForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private tokenStorage: TokenStorage,
              private utilisateurService: UtilisateurService,) { }

  ngOnInit() {

    this.validateForm = this.fb.group({
      emailOrUsername: [null, [Validators.required]],
      newPassword: [null],
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.valid === true) {
      const formData = this.validateForm.value;
      console.log(formData);
        this.utilisateurService.forgetPassword(formData).subscribe(
          (data: any) => {
            console.log(data);
            if (data && data.success == true) {
              Swal.fire(
                "Success!",
                data.message,
                "success"
              );
              this.router.navigate(['/auth']);
            } else {
              this.isErrorAuthentication = true;
              this.isErrorMessage = data.message;
            }
          },
          (error: HttpErrorResponse) => {
            console.log(error?error.status : 'erreur');
          });
    } else {
      this.isErrorAuthentication = true;
      this.isErrorMessage = 'Veuillez remplir correctement les champs.';
    }

  }

}
