import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";
import {TokenStorage} from "../../utilitaires/token-storage";
import {HttpErrorResponse} from "@angular/common/http";
import {UtilisateurService} from "../../services/back-office/utilisateur.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {

  isErrorAuthentication = false;
  isErrorMessage = '';

  validateForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private tokenStorage: TokenStorage,
              private utilisateurService: UtilisateurService,) { }

  ngOnInit() {

    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      repeatPassword: [null, [Validators.required]],
      username: [null, [Validators.required]],
      name: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.valid === true) {
      const formData = this.validateForm.value;
      console.log(formData);
      if(formData.password == formData.repeatPassword) {
        this.utilisateurService.inscription(formData).subscribe(
          (data: any) => {
            console.log(data);
            if (data && data.success == true) {
              Swal.fire(
                "Success!",
                data.message,
                "success"
              );
              this.router.navigate(['/auth/inscription']);
            } else {
              this.isErrorAuthentication = true;
              this.isErrorMessage = data.message;
            }
          },
          (error: HttpErrorResponse) => {
            console.log(error?error.status : 'erreur');
          });
      } else {
        this.isErrorAuthentication = true;
        this.isErrorMessage = 'Confirmation de mot de passe erroné.';
      }

    } else {
      this.isErrorAuthentication = true;
      this.isErrorMessage = 'Veuillez remplir correctement les champs.';
    }

  }

}
