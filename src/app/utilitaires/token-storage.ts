import {Injectable} from "@angular/core";
import {Utilisateur} from "../models/auth/utilisateur";
import {JwtHelperService} from "@auth0/angular-jwt";
import {environment} from "../../environments/environment";

const TOKEN_KEY = 'AuthToken';

@Injectable()
export class TokenStorage {

    expirationsession: number;

    constructor() {
        this.expirationsession = new Date().getTime() + environment.expirationTime;
    }

    signOut() {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.clear();
    }

    public saveToken(token: string) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }

    public getToken(): string {
        return sessionStorage.getItem(TOKEN_KEY);
    }

    public getUser(): Utilisateur {
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(this.getToken());
        //console.log(decodedToken);
        return decodedToken != null ? decodedToken.User : null;
    }
}
