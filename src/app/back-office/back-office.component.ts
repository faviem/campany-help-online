import { Component, OnInit } from '@angular/core';
import {Utilisateur} from "../models/auth/utilisateur";
import {Router} from "@angular/router";
import {TokenStorage} from "../utilitaires/token-storage";
import {StatistiqueService} from "../services/back-office/statistique.service";
import {Question} from "../models/back-office/question";
import {ResponseBody} from "../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";

export interface Statistique {
  nombreQuestionsEnAttenteReponse:number;
  nombrePublicationGlobal: number;
  nombreQuestionGlobal: number;
  nombreQuestionParDepartement: number;
  nombreReponseGlobal: number;
  nombreReponseParDepartement: number;
  nombrePublicationParCategorie: number;
  list10DerniereQuestionsEnAttente: Question[];
}
@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: ['./back-office.component.css']
})
export class BackOfficeComponent implements OnInit {

  user: Utilisateur = null;

  statitstiqueData: Statistique = null;

  constructor(
    private router: Router,
    private tokenStorage: TokenStorage,
    private statistiqueService: StatistiqueService,
  ) { }

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    // console.log(this.user);
    this.statistiqueService.getByStatistiqueList(null).subscribe(
      (data: ResponseBody) => {
        this.statitstiqueData = data.object;
        console.log(this.statitstiqueData);
      },
      (error: HttpErrorResponse) => {
      });
  }

  logout() {
    this.tokenStorage.signOut();
    this.user = null;
    this.router.navigate(['/auth']);
  }

  linkToExternalSite(): void {

  }

}
