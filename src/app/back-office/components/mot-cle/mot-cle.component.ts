import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Table} from "primeng/table";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";
import {MotCle} from "../../../models/back-office/mot-cle";
import {MotCleService} from "../../../services/back-office/mot-cle.service";

@Component({
  selector: 'app-mot-cle',
  templateUrl: './mot-cle.component.html',
  styleUrls: ['./mot-cle.component.css']
})
export class MotCleComponent implements OnInit {

  motCleForm: FormGroup;
  motCleList: MotCle[] = [];
  displayedForm: boolean = false;
  motCle: MotCle = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private motCleService: MotCleService
  ) {
  }

  ngOnInit(): void {
    this.makeForm(null);
    this.list();
  }

  makeForm(motCle: MotCle): void {
    this.motCleForm = this.fb.group({
      id: [motCle != null ? motCle.id : null],
      libelle: [motCle != null ? motCle.libelle : null,
        [Validators.required]],
    });
  }

  deleteMotCleOnList(i: number, motCle: MotCle): void {
    this.makeForm(null);
    if (i == 1) {
      this.motCle = motCle;
    }
    if (i == 2) {
      this.motCleService
        .supprimer(motCle.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.motCleList.findIndex((c) => c.id == motCle.id);
            if (i > -1) {
              this.motCleList.splice(i, 1);
              this.motCleList = [...this.motCleList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.motCle = null;
    }
  }

  list(): void {
    this.loading = true;
    this.motCleService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.motCleList = data.object != null ? data.object : [];
        this.motCleList = [...this.motCleList];
        console.log(this.motCleList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  submitForm(): void {
    for (const i in this.motCleForm.controls) {
      this.motCleForm.controls[i].markAsDirty();
      this.motCleForm.controls[i].updateValueAndValidity();
    }

    if (this.motCleForm.valid === true) {
      const formData = this.motCleForm.value;
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.motCleService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.motCleList.findIndex((c) => c.id == formData.id);
              this.motCleList[i] = data.object;
              this.motCleList = [...this.motCleList];
              this.motCleForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.motCleService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.motCleList.unshift(data.object);
              this.motCleList = [...this.motCleList];
              this.motCleForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

}
