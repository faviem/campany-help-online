import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Table} from "primeng/table";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";
import {InfoYoutube} from "../../../models/back-office/info-youtube";
import {InfoYoutubeService} from "../../../services/back-office/info-youtube.service";

@Component({
  selector: 'app-info-youtube',
  templateUrl: './info-youtube.component.html',
  styleUrls: ['./info-youtube.component.css']
})
export class InfoYoutubeComponent implements OnInit {

  infoYoutubeForm: FormGroup;
  infoYoutubeList: InfoYoutube[] = [];
  displayedForm: boolean = false;
  infoYoutube: InfoYoutube = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private infoYoutubeService: InfoYoutubeService
  ) {
  }

  ngOnInit(): void {
    this.makeForm(null);
    this.list();
  }

  makeForm(infoYoutube: InfoYoutube): void {
    this.infoYoutubeForm = this.fb.group({
      id: [infoYoutube != null ? infoYoutube.id : null],
      titre: [infoYoutube != null ? infoYoutube.titre : null,
        [Validators.required]],
      description: [infoYoutube != null ? infoYoutube.titre : null,
        [Validators.required]],
      lienYoutube: [infoYoutube != null ? infoYoutube.titre : null,
        [Validators.required]],
      laUne: [infoYoutube != null ? infoYoutube.titre : false],
    });
  }

  deleteInfoYoutubeOnList(i: number, infoYoutube: InfoYoutube): void {
    this.makeForm(null);
    if (i == 1) {
      this.infoYoutube = infoYoutube;
    }
    if (i == 2) {
      this.infoYoutubeService
        .supprimer(infoYoutube.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.infoYoutubeList.findIndex((c) => c.id == infoYoutube.id);
            if (i > -1) {
              this.infoYoutubeList.splice(i, 1);
              this.infoYoutubeList = [...this.infoYoutubeList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.infoYoutube = null;
    }
  }

  list(): void {
    this.loading = true;
    this.infoYoutubeService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.infoYoutubeList = data.object != null ? data.object : [];
        this.infoYoutubeList = [...this.infoYoutubeList];
        console.log(this.infoYoutubeList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  submitForm(): void {
    for (const i in this.infoYoutubeForm.controls) {
      this.infoYoutubeForm.controls[i].markAsDirty();
      this.infoYoutubeForm.controls[i].updateValueAndValidity();
    }

    if (this.infoYoutubeForm.valid === true) {
      const formData = this.infoYoutubeForm.value;
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.infoYoutubeService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.infoYoutubeList.findIndex((c) => c.id == formData.id);
              this.infoYoutubeList[i] = data.object;
              this.infoYoutubeList = [...this.infoYoutubeList];
              this.infoYoutubeForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.infoYoutubeService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.infoYoutubeList.unshift(data.object);
              this.infoYoutubeList = [...this.infoYoutubeList];
              this.infoYoutubeForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

}
