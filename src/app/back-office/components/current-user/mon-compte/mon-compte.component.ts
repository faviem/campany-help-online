import { Component, OnInit } from '@angular/core';
import {Utilisateur} from "../../../../models/auth/utilisateur";
import {Router} from "@angular/router";
import {TokenStorage} from "../../../../utilitaires/token-storage";

@Component({
  selector: 'app-mon-compte',
  templateUrl: './mon-compte.component.html',
  styleUrls: ['./mon-compte.component.css']
})
export class MonCompteComponent implements OnInit {

  user: Utilisateur = null;

  constructor(
    private router: Router,
    private tokenStorage: TokenStorage,
  ) { }

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    console.log(this.user);
  }

  getRoleLibelle(roleName: string): string {
    if(roleName === 'ROLE_ADMIN') {
      return 'ADMINISTRATEUR';
    }
    return 'ACTEUR CONTRIBUATEUR';
  }

}
