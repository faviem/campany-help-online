import { Component, OnInit } from '@angular/core';
import {Question} from "../../../../models/back-office/question";
import {Utilisateur} from "../../../../models/auth/utilisateur";
import {Departement} from "../../../../models/back-office/departement";
import {QuestionService} from "../../../../services/back-office/question.service";
import {Router} from "@angular/router";
import {TokenStorage} from "../../../../utilitaires/token-storage";
import {UtilisateurService} from "../../../../services/back-office/utilisateur.service";
import {DepartementService} from "../../../../services/back-office/departement.service";
import {ResponseBody} from "../../../../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";
import Swal from "sweetalert2";

@Component({
  selector: 'app-mes-questions',
  templateUrl: './mes-questions.component.html',
  styleUrls: ['./mes-questions.component.css']
})
export class MesQuestionsComponent implements OnInit {

  questionList: Question[] = [];
  user: Utilisateur = null;
  loading = false;
  question: Question = null;
  departement: Departement = null;
  departementList: Departement[] = [];
  totalRecord = 0;

  constructor(
    private questionService: QuestionService,
    private router: Router,
    private tokenStorage: TokenStorage,
    private utilisateurService: UtilisateurService,
    private departementService: DepartementService
  ) {
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    this.departementService.liste().subscribe(
      (data: ResponseBody) => {
        this.departementList = data.object != null ? data.object : [];
      },
      (error: HttpErrorResponse) => {
      });
    this.list({page: 0});
  }

  list(event: any): void {
    this.loading = true;
    this.utilisateurService
      .listeQuestionForUser(event.page, 10)
      .subscribe(
        (data: ResponseBody) => {
          this.loading = false;
          this.questionList = data != null && data.object != null ? data.object.content : [];
          this.questionList = [...this.questionList];
          this.totalRecord = data.object != null ? data.object.totalElements : 0;
          console.log('this.questionList', this.questionList);
        },
        (error: HttpErrorResponse) => {
          this.loading = false;
        });
  }

  repondreQuestion(question: Question): void {
    this.question = question;
  }

  annulerAffecter(): void {
    this.question = null;
  }

}
