import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UtilisateurService} from "../../../../services/back-office/utilisateur.service";
import Swal from "sweetalert2";
import {ResponseBody} from "../../../../models/response-body";
import {Router} from "@angular/router";


@Component({
  selector: 'app-reinitialyze-password',
  templateUrl: './reinitialyze-password.component.html',
  styleUrls: ['./reinitialyze-password.component.css']
})
export class ReinitialyzePasswordComponent implements OnInit {

  utilisateurForm: FormGroup;
  isEnregistrement: boolean = false;

  constructor(
    private fb: FormBuilder,
    private utilisateurService: UtilisateurService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.makeForm();

  }

  makeForm(): void {
    this.utilisateurForm = this.fb.group({
      newPassword: [null, [Validators.required]],
      confirmation: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    for (const i in this.utilisateurForm.controls) {
      this.utilisateurForm.controls[i].markAsDirty();
      this.utilisateurForm.controls[i].updateValueAndValidity();
    }

    if (this.utilisateurForm.valid === true) {

      const formData = this.utilisateurForm.value;

      if (formData.newPassword != formData.confirmation) {
        Swal.fire("Erreur", "Confirmation du mot de passe erroné !", "error");
      } else {
        delete formData.confirmation;
        this.isEnregistrement = true;
        this.utilisateurService
          .reinitialiserPassword(formData)
          .subscribe((data: ResponseBody) => {
                console.log(data);
                if(data.success) {
                  Swal.fire(
                    "Success!",
                    data.message,
                    "success"
                  );
                  this.router.navigate(['/auth']);
                } else {
                  Swal.fire("Erreur", data.message, "error");
                }
            this.isEnregistrement = false;
            });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalid !", "error");
      this.isEnregistrement = false;
    }
  }

}
