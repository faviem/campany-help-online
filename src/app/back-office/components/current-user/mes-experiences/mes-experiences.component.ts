import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Publication} from "../../../../models/back-office/publication";
import {MotCle} from "../../../../models/back-office/mot-cle";
import {Media} from "../../../../models/back-office/media";
import {Categorie} from "../../../../models/back-office/categorie";
import {Table} from "primeng/table";
import {PublicationSearch} from "../../../../models/back-office/publication-search";
import {PublicationService} from "../../../../services/back-office/publication.service";
import {MotCleService} from "../../../../services/back-office/mot-cle.service";
import {CategorieService} from "../../../../services/back-office/categorie.service";
import {MediaService} from "../../../../services/back-office/media.service";
import {ResponseBody} from "../../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-mes-experiences',
  templateUrl: './mes-experiences.component.html',
  styleUrls: ['./mes-experiences.component.css']
})
export class MesExperiencesComponent implements OnInit {

  publicationForm: FormGroup;
  publicationList: Publication[] = [];
  displayedForm: boolean = false;
  publication: Publication = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  motCleList: MotCle[] = [];
  mediaList: Media[] = [];
  categorieList: Categorie[] = [];
  statutList = ['ANNULE', 'BROUILLON', 'EN_REVISION', 'PUBLIE'];

  @ViewChild("dt", {static: false}) table: Table;

  /* image à la une et autre médias ressource */
  mediaRessourceList: Media[] = [];
  imageUne: Media = null;
  media: Media = null;
  @ViewChild("fileInput") fileInput: ElementRef;
  fileToSave: File = null;
  /* fin déclaration */

  /* mot clé à ajouter */
  motCleTyping: string = null;
  /* fin mot clé */

  isPublicationTyping: boolean = false;
  publicationSearch: PublicationSearch = new PublicationSearch();
  totalRecord = 0;
  searchForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private publicationService: PublicationService,
    private motCleService: MotCleService,
    private categorieService: CategorieService,
    private mediaService: MediaService
  ) {
  }

  ngOnInit(): void {
    this.listParameters();
    this.makeForm(false, null);
    this.list({page : 0});
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  makeForm(isPublicationTyping: boolean, publication: Publication): void {
    this.isPublicationTyping = isPublicationTyping;
    this.publicationForm = this.fb.group({
      id: [publication != null ? publication.id : null],
      categories: [publication != null ? publication.categories : null,
        [Validators.required]],
      titre: [publication != null ? publication.titre : null,
        [Validators.required]],
      contenu: [publication != null ? publication.contenu : null,
        [Validators.required]],
      imageEnAvant: [publication != null ? publication.imageEnAvant : null],
      motCles: [publication != null ? publication.motCles : null],
      ressources: [publication != null ? publication.ressources : null],
      slug: [publication != null ? publication.slug : null],
      status: [publication != null ? publication.status : null],
    });

    if (publication != null) {
      this.imageUne = publication.imageEnAvant;
      this.mediaRessourceList = publication.ressources;
      this.mediaRessourceList = [...this.mediaRessourceList];
    } else {
      this.imageUne = null;
      this.mediaRessourceList = [];
    }
  }

  newPublicationFormDisplayed(): void {
    this.makeForm(true, null);
  }

  displayDetailPublication(publication: Publication): void {
    this.publication = publication;
  }

  closeDetailPublication(): void {
    this.publication = null;
  }

  deletePublicationOnList(i: number, publication: Publication): void {
    this.makeForm(false, null);
    if (i == 1) {
      this.publication = publication;
    }
    if (i == 2) {
      this.publicationService
        .supprimer(publication.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.publicationList.findIndex((c) => c.id == publication.id);
            if (i > -1) {
              this.publicationList.splice(i, 1);
              this.publicationList = [...this.publicationList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.publication = null;
    }
  }

  list(event: any): void {
    this.loading = true;
    this.publicationService.getListeByCurrentUser(event.page, 10).subscribe(
      (data: ResponseBody) => {
        console.log(data);
        this.loading = false;
        this.publicationList = data.object != null ? data.object.publications : [];
        this.publicationList = [...this.publicationList];
        this.totalRecord = data.object != null ? data.object.totalElements : 0;
        console.log(this.publicationList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  submitForm(): void {
    for (const i in this.publicationForm.controls) {
      this.publicationForm.controls[i].markAsDirty();
      this.publicationForm.controls[i].updateValueAndValidity();
    }

    const formData = this.publicationForm.value;
    formData.ressources = this.mediaRessourceList;
    formData.imageEnAvant = this.imageUne;
    formData.status = 'BROUILLON';
    console.log(formData);

    if (this.publicationForm.valid === true) {
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.publicationService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.publicationList.findIndex((c) => c.id == formData.id);
              this.publicationList[i] = data.object;
              this.publicationList = [...this.publicationList];
              this.publicationForm.reset();
              this.makeForm(false, null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.publicationService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.publicationList.unshift(data.object);
              this.publicationList = [...this.publicationList];
              this.publicationForm.reset();
              this.makeForm(true, null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

  listParameters(): void {
    this.motCleService.liste().subscribe(
      (data: ResponseBody) => {
        this.motCleList = data.object != null ? data.object : [];
      });
    this.categorieService.liste().subscribe(
      (data: ResponseBody) => {
        this.categorieList = data.object != null ? data.object.filter((d) => d.experiencePartage == true) : [];
      });
    this.mediaService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.mediaList = data.object != null ? data.object : [];
      });
  }

  /* gestion des médias */
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.fileToSave = event.target.files[0];
    }
  }

  ajouterImageUne(event) {
    if (event.target.files.length > 0) {
      this.mediaService.enregistrer(event.target.files[0]).subscribe(
        (data: any) => {
          console.log(data);
          if (data.body.success == true) {
            Swal.fire("Information", data.body.message, "info");
            this.imageUne = data.body.object;
          } else {
            Swal.fire("Erreur", data.body.message, "error");
          }
        });
    }
  }

  annulerImageUneFile(): void {
    this.deleteFileOnList(2, this.imageUne);
  }

  annulerRessourceFile(): void {
    this.fileInput.nativeElement.value = "";
    this.fileToSave = null;
  }

  addPieceToRessource(event): void {
    console.log(event);
    this.mediaRessourceList.unshift(event);
    this.mediaRessourceList = [...this.mediaRessourceList];
  }

  deleteFileOnList(i: number, media: Media): void {
    if (i == 1) {
      this.media = media;
    }
    if (i == 2) {
      const i = this.mediaRessourceList.findIndex((c) => c.id == media.id);
      if (i > -1) {
        this.mediaRessourceList.splice(i, 1);
        this.mediaRessourceList = [...this.mediaRessourceList];
      }
      if (this.imageUne != null && media.id == this.imageUne.id) {
        this.imageUne = null;
      }
      Swal.fire(
        "Success!",
        "Suppression effectuée !",
        "success"
      );
    }
  }

  submitFileForm(): void {
    if (this.fileToSave != null) {
      this.mediaService.enregistrer(this.fileToSave).subscribe(
        (data: any) => {
          console.log(data);
          if (data.body.success == true) {
            Swal.fire("Information", data.body.message, "info");
            this.mediaList.unshift(data.body.object);
            this.mediaRessourceList.unshift(data.body.object);
            this.mediaList = [...this.mediaList];
            this.mediaRessourceList = [...this.mediaRessourceList];
            this.annulerRessourceFile();
          } else {
            Swal.fire("Erreur", data.body.message, "error");
          }
        });
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

  printFileToViewInNavigator(filename: string): void {
    this.mediaService.telecharger(filename).subscribe((res: any) => {
      console.log("Nom du fichier2: ", res);
      var file, fileURL;
      file = new Blob([res], {type: res.type});
      fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, (error: any) => {
      console.log(error);
      Swal.fire("Erreur", "Téléchargement du fichier impossible.", "error");
    });
  }

  /* fin gestion des médias */

  /* ajout de mot clé dynamiquement */
  ajouterMotClePublication(): void {
    if (this.motCleTyping != null && this.motCleTyping != '') {
      const formData = {id: null, libelle: this.motCleTyping}
      this.motCleService.enregistrer(formData).subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == false) {
            Swal.fire("Erreur", data.message, "error");
          } else {
            this.motCleList.unshift(data.object);
            this.motCleList = [...this.motCleList];
            let motcles: MotCle[] = this.publicationForm.get('motCles').value;
            if (motcles != null) {
              motcles.unshift(data.object);
            } else {
              motcles = [data.object];
            }
            this.publicationForm.get('motCles').setValue(motcles);
            this.motCleTyping = null;
            Swal.fire("Information", data.message, "info");
          }
        },
        (error: HttpErrorResponse) => {
          Swal.fire("Erreur", "Problème système !", "error");
        });
    } else {
      Swal.fire("Erreur", "Aucun mot clé n'est enregistré !", "error");
    }
  }

  /* fin ajout de mot clé */
  onFilterNameChange(event, theadKey) {
    console.log(event);
    this.table.filter(
      event.value.map((element) => element.id),
      theadKey,
      'in'
    );
  }

  copyToClipBoard(textToCopy): void {
    navigator.clipboard.writeText(textToCopy)
      .then(() => {
        console.log("Text copied to clipboard...");
        alert("Lien copié !");
      })
  }

}
