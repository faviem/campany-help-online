import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Table} from "primeng/table";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";
import {UtilisateurService} from "../../../services/back-office/utilisateur.service";
import {Role} from "../../../models/auth/role";
import {Departement} from "../../../models/back-office/departement";
import {Utilisateur} from "../../../models/auth/utilisateur";
import {DepartementService} from "../../../services/back-office/departement.service";
import {StatistiqueService} from "../../../services/back-office/statistique.service";

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

  utilisateurForm: FormGroup;
  utilisateurList: Utilisateur[] = [];
  displayedForm: boolean = false;
  utilisateur: Utilisateur = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;
  departementList: Departement[] = [];
  roleList: Role[] = [];

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private utilisateurService: UtilisateurService,
    private departementService: DepartementService,
    private statistiqueService: StatistiqueService,
  ) {
  }

  ngOnInit(): void {
    this.departementService.liste().subscribe(
      (data: ResponseBody) => {
        this.departementList = data.object != null ? data.object : [];
      });
    this.statistiqueService.listeRoles().subscribe(
      (data: ResponseBody) => {
        this.roleList = data.object != null ? data.object : [];
      });
    this.makeForm(null);
    this.list();
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  makeForm(utilisateur: Utilisateur): void {
    this.utilisateurForm = this.fb.group({
      id: [utilisateur != null ? utilisateur.id : null],
      name: [utilisateur != null ? utilisateur.name : null,
        [Validators.required]],
      username: [utilisateur != null ? utilisateur.username : null,
        [Validators.required]],
      email: [utilisateur != null ? utilisateur.email : null,
        [Validators.required]],
      password: [null],
      departement: [utilisateur != null ? utilisateur.departement : null],
      roles: [utilisateur != null ? utilisateur.roles[0] : null]
    });
  }

  deleteUtilisateurOnList(i: number, utilisateur: Utilisateur): void {
    this.makeForm(null);
    if (i == 1) {
      this.utilisateur = utilisateur;
    }
    if (i == 2) {
      this.utilisateurService
        .supprimer(utilisateur.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.utilisateurList.findIndex((c) => c.id == utilisateur.id);
            if (i > -1) {
              this.utilisateurList.splice(i, 1);
              this.utilisateurList = [...this.utilisateurList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.utilisateur = null;
    }
  }

  list(): void {
    this.loading = true;
    this.utilisateurService.liste().subscribe(
      (data: any) => {
        this.loading = false;
        this.utilisateurList = data != null ? data : [];
        if(this.utilisateurList != null && this.utilisateurList.length >0) {
          this.utilisateurList.forEach((utilisateur, index) => {
            this.utilisateurList[index].groupe = utilisateur.roles[0];
          });
        }
        this.utilisateurList = [...this.utilisateurList];
        console.log(this.utilisateurList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }


  submitForm(): void {
    for (const i in this.utilisateurForm.controls) {
      this.utilisateurForm.controls[i].markAsDirty();
      this.utilisateurForm.controls[i].updateValueAndValidity();
    }

    if (this.utilisateurForm.valid === true) {
      const formData = this.utilisateurForm.value;
      formData.roles = [formData.roles];
      console.log('formData', formData);
      this.isEnregistrement = true;
      if (formData.id != null) {
        delete formData.password;
        this.utilisateurService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.utilisateurList.findIndex((c) => c.id == formData.id);
              this.utilisateurList[i] = data.object;
              this.utilisateurList[i].groupe =  this.utilisateurList[i].roles[0];
              this.utilisateurList = [...this.utilisateurList];
              this.utilisateurForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.utilisateurService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const utilisateur: Utilisateur = data.object;
              utilisateur.groupe = utilisateur.roles[0];
              this.utilisateurList.unshift(utilisateur);
              this.utilisateurList = [...this.utilisateurList];
              this.utilisateurForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

  onFilterNameChange(event, theadKey) {
    console.log(event);
    this.table.filter(
      event.value.map((element) => element.id),
      theadKey,
      'in'
    );
  }

}
