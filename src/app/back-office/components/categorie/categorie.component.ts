import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Categorie} from "../../../models/back-office/categorie";
import {Table} from "primeng/table";
import {CategorieService} from "../../../services/back-office/categorie.service";
import Swal from 'sweetalert2'
import {ResponseBody} from "../../../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";
import {MediaService} from "../../../services/back-office/media.service";
import {Media} from "../../../models/back-office/media";

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {

  categorieForm: FormGroup;
  categorieList: Categorie[] = [];
  mediaList: Media[] = [];
  displayedForm: boolean = false;
  categorie: Categorie = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  /* image à la une et autre médias ressource */
  imageUne: Media = null;
  media: Media = null;
  @ViewChild("fileInput") fileInput: ElementRef;
  fileToSave: File = null;
  /* fin déclaration */

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private categorieService: CategorieService,
    private mediaService: MediaService
  ) {
  }

  ngOnInit(): void {
    this.mediaService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.mediaList = data.object != null ? data.object : [];
      });
    this.makeForm(null);
    this.list();
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  makeForm(categorie: Categorie): void {
    this.categorieForm = this.fb.group({
      id: [categorie != null ? categorie.id : null],
      libelle: [categorie != null ? categorie.libelle : null,
        [Validators.required]],
      code: [categorie != null ? categorie.code : null,
        [Validators.required]],
      description: [categorie != null ? categorie.code : null,
        [Validators.required]],
      publier: [categorie != null ? categorie.experiencePartage : false],
      experiencePartage: [categorie != null ? categorie.experiencePartage : false],
      imageAlaUne: [categorie != null ? categorie.imageAlaUne : null],
    });
  }

  deleteCategorieOnList(i: number, categorie: Categorie): void {
    this.makeForm(null);
    if (i == 1) {
      this.categorie = categorie;
    }
    if (i == 2) {
      this.categorieService
        .supprimer(categorie.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.categorieList.findIndex((c) => c.id == categorie.id);
            if (i > -1) {
              this.categorieList.splice(i, 1);
              this.categorieList = [...this.categorieList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.categorie = null;
    }
  }

  list(): void {
    this.loading = true;
    this.categorieService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.categorieList = data.object != null ? data.object : [];
        this.categorieList = [...this.categorieList];
        console.log(this.categorieList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }


  submitForm(): void {
    for (const i in this.categorieForm.controls) {
      this.categorieForm.controls[i].markAsDirty();
      this.categorieForm.controls[i].updateValueAndValidity();
    }

    if (this.categorieForm.valid === true) {
      const formData = this.categorieForm.value;
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.categorieService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.categorieList.findIndex((c) => c.id == formData.id);
              this.categorieList[i] = data.object;
              this.categorieList = [...this.categorieList];
              this.categorieForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.categorieService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.categorieList.unshift(data.object);
              this.categorieList = [...this.categorieList];
              this.categorieForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }


  /* gestion des médias */
  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.fileToSave = event.target.files[0];
    }
  }

  annulerRessourceFile(): void {
    this.fileInput.nativeElement.value = "";
    this.fileToSave = null;
  }

  submitFileForm(): void {
    if (this.fileToSave != null) {
      this.mediaService.enregistrer(this.fileToSave).subscribe(
        (data: any) => {
          console.log(data);
          if (data.body.success == true) {
            Swal.fire("Information", data.body.message, "info");
            this.mediaList.unshift(data.body.object);
            this.categorieForm.get('imageAlaUne').setValue(data.body.object);
            this.annulerRessourceFile();
          } else {
            Swal.fire("Erreur", data.body.message, "error");
          }
        });
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

  printFileToViewInNavigator(): void {
    const media = this.categorieForm.get('imageAlaUne').value;
    if(media != null) {
      this.mediaService.telecharger(media.fileName).subscribe((res: any) => {
        console.log("Nom du fichier2: ", res);
        var file, fileURL;
        file = new Blob([res], {type: res.type});
        fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      }, (error: any) => {
        console.log(error);
        Swal.fire("Erreur", "Téléchargement du fichier impossible.", "error");
      });
    } else {
      Swal.fire("Erreur", "Média non choisi.", "error");
    }

  }


}
