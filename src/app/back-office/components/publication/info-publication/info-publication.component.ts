import {Component, Input, OnInit} from '@angular/core';
import {Publication} from "../../../../models/back-office/publication";
import Swal from "sweetalert2";
import {MediaService} from "../../../../services/back-office/media.service";

@Component({
  selector: 'app-info-publication',
  templateUrl: './info-publication.component.html',
  styleUrls: ['./info-publication.component.css']
})
export class InfoPublicationComponent implements OnInit {

  @Input() publication: Publication = null;

  constructor(
    private mediaService: MediaService
  ) { }

  ngOnInit(): void {
  }

  printFileToViewInNavigator(filename: string): void {
    this.mediaService.telecharger(filename).subscribe((res: any) => {
      console.log("Nom du fichier2: ", res);
      var file, fileURL;
      file = new Blob([res], {type: res.type});
      fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, (error: any) => {
      console.log(error);
      Swal.fire("Erreur", "Téléchargement du fichier impossible.", "error");
    });
  }


  copyToClipBoard(textToCopy): void {
    navigator.clipboard.writeText(textToCopy)
      .then(() => {
        console.log("Text copied to clipboard...");
        alert("Lien copié !");
      })
  }

}
