import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Media} from "../../../models/back-office/media";
import {Table} from "primeng/table";
import {MediaService} from "../../../services/back-office/media.service";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-medias',
  templateUrl: './medias.component.html',
  styleUrls: ['./medias.component.css']
})
export class MediasComponent implements OnInit {

  mediaList: Media[] = [];
  media: Media = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  @ViewChild("fileInput") fileInput: ElementRef;
  fileToSave: File = null;

  constructor(
    private fb: FormBuilder,
    private mediaService: MediaService
  ) {
  }

  ngOnInit(): void {
    this.list();
  }

  deleteMediaOnList(i: number, media: Media): void {
    if (i == 1) {
      this.media = media;
    }
    if (i == 2) {
      this.mediaService
        .supprimer(media.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.mediaList.findIndex((c) => c.id == media.id);
            if (i > -1) {
              this.mediaList.splice(i, 1);
              this.mediaList = [...this.mediaList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.media = null;
    }
  }

  list(): void {
    this.loading = true;
    this.mediaService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.mediaList = data.object != null ? data.object : [];
        this.mediaList = [...this.mediaList];
        console.log(this.mediaList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.fileToSave = event.target.files[0];
    }
  }

  annuler(): void  {
    this.fileInput.nativeElement.value = "";
    this.fileToSave = null;
  }

  submitForm(): void {
    if(this.fileToSave != null) {
      this.mediaService.enregistrer(this.fileToSave).subscribe(
        (data: any) => {
          console.log(data);
          if (data.body.success == true) {
            Swal.fire("Information", data.body.message, "info");
            this.mediaList.unshift(data.body.object);
            this.mediaList = [...this.mediaList];
            this.annuler();

          } else {
            Swal.fire("Erreur", data.body.message, "error");
          }
        });
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

  printFileToViewInNavigator(filename: string): void {
    this.mediaService.telecharger(filename).subscribe((res: any) => {
      console.log("Nom du fichier2: ",  res);
      var file, fileURL;
      file = new Blob([res], {type: res.type});
      fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, (error: any)=>{
      console.log(error);
      Swal.fire("Erreur", "Téléchargement du fichier impossible.", "error");
    });
  }

  copyToClipBoard(textToCopy): void {
    navigator.clipboard.writeText(textToCopy)
      .then(() => {
        console.log("Text copied to clipboard...");
        alert("Lien copié !");
      })
  }

}
