import {Component, OnInit} from '@angular/core';
import {Question} from "../../../models/back-office/question";
import {QuestionService} from "../../../services/back-office/question.service";
import {Router} from "@angular/router";
import {TokenStorage} from "../../../utilitaires/token-storage";
import {Utilisateur} from "../../../models/auth/utilisateur";
import {ResponseBody} from "../../../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";
import {UtilisateurService} from "../../../services/back-office/utilisateur.service";
import Swal from "sweetalert2";
import {DepartementService} from "../../../services/back-office/departement.service";
import {Departement} from "../../../models/back-office/departement";

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  questionList: Question[] = [];
  user: Utilisateur = null;
  loading = false;
  question: Question = null;
  departement: Departement = null;
  departementList: Departement[] = [];
  totalRecord = 0;

  constructor(
    private questionService: QuestionService,
    private router: Router,
    private tokenStorage: TokenStorage,
    private utilisateurService: UtilisateurService,
    private departementService: DepartementService
  ) {
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    this.departementService.liste().subscribe(
      (data: ResponseBody) => {
        this.departementList = data.object != null ? data.object : [];
      },
      (error: HttpErrorResponse) => {
      });
    this.list({page: 0});
  }

  list(event: any): void {
    this.loading = true;
    this.utilisateurService
      .listeQuestionForDepartementOfCurrentUserSansReponse(event.page, 10)
      .subscribe(
        (data: ResponseBody) => {
          this.loading = false;
          this.questionList = data != null && data.object != null ? data.object.content : [];
          this.questionList = [...this.questionList];
          this.totalRecord = data.object != null ? data.object.totalElements : 0;
          console.log(this.questionList);
        },
        (error: HttpErrorResponse) => {
          this.loading = false;
        });
  }

  repondreQuestion(question: Question): void {
    this.question = question;
  }

  annulerAffecter(): void {
    this.departement = null;
    this.question = null;
  }

  affecterQuestion(i: number, question: Question): void {
    if (i == 1) {
      this.question = question;
    }
      if (i == 2) {
        if(this.departement != null) {
        this.questionService
          .affecter({ idDepartement: this.departement?.id, idQuestion: this.question?.id  })
          .subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == true) {
              const i = this.questionList.findIndex((c) => c.id == question.id);
              if (i > -1) {
                this.questionList[i] = data.object;
                this.questionList = [...this.questionList];
              }
              Swal.fire(
                "Success!",
                data.message,
                "success"
              );
            } else {
              Swal.fire("Erreur", data.message, "error");
            }
          });
        this.question = null;
        this.departement = null;
      } else {
          Swal.fire("Erreur", "Département inexistant !", "error");
        }
      }
  }

  deleteQuestionOnList(i: number, question: Question): void {
    if (i == 1) {
      this.question = question;
    }
    if (i == 2) {
      this.questionService
        .supprimer(question.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.questionList.findIndex((c) => c.id == question.id);
            if (i > -1) {
              this.questionList.splice(i, 1);
              this.questionList = [...this.questionList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.question = null;
    }
  }

}
