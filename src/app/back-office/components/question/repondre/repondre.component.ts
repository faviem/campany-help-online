import {Component, Input, OnInit} from '@angular/core';
import {Question} from "../../../../models/back-office/question";
import {ReponseService} from "../../../../services/back-office/reponse.service";
import {Reponse} from "../../../../models/back-office/reponse";
import {ResponseBody} from "../../../../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";
import Swal from "sweetalert2";

@Component({
  selector: 'app-repondre',
  templateUrl: './repondre.component.html',
  styleUrls: ['./repondre.component.css']
})
export class RepondreComponent implements OnInit {

  @Input() question: Question;
  @Input() formDisplaying: boolean = false;

  reponseList: Reponse[] = [];
  loading = false;
  isEnregistrement = false;
  reponseTyping: string = '';

  constructor(
    private reponseService: ReponseService,
  ) { }

  ngOnInit(): void {
    if(this.question != null) {
      this.loading = true;
      this.reponseService
        .getListeByQuestion(this.question?.id)
        .subscribe(
          (data: ResponseBody) => {
            this.loading = false;
            this.reponseList = data.object != null ? data.object : [];
            this.reponseList = [...this.reponseList];
            console.log(this.reponseList);
          },
          (error: HttpErrorResponse) => {
            this.loading = false;
          });
    }
  }

  annuler(): void {
    this.reponseTyping = '';
  }

  enregistrerReponse(): void {
    if (this.reponseTyping != '' && this.question != null) {
      this.isEnregistrement = true;
      const formData = { question: this.question, reponse: {contenu: this.reponseTyping, id: null } };
        this.reponseService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.reponseList.unshift(data.object);
              this.reponseList = [...this.reponseList];
              this.reponseTyping = '';
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
    } else {
      Swal.fire("Erreur", "Entrer obligatoirement la réponse.", "error");
    }
  }

}
