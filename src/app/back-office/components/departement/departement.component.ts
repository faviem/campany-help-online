import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Departement} from "../../../models/back-office/departement";
import {Table} from "primeng/table";
import {DepartementService} from "../../../services/back-office/departement.service";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-departement',
  templateUrl: './departement.component.html',
  styleUrls: ['./departement.component.css']
})
export class DepartementComponent implements OnInit {

  departementForm: FormGroup;
  departementList: Departement[] = [];
  displayedForm: boolean = false;
  departement: Departement = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private departementService: DepartementService
  ) {
  }

  ngOnInit(): void {
    this.makeForm(null);
    this.list();
  }

  makeForm(departement: Departement): void {
    this.departementForm = this.fb.group({
      id: [departement != null ? departement.id : null],
      libelle: [departement != null ? departement.libelle : null,
        [Validators.required]],
      departementParDefaut: [departement != null ? departement.departementParDefaut : false],
    });
  }

  deleteDepartementOnList(i: number, departement: Departement): void {
    this.makeForm(null);
    if (i == 1) {
      this.departement = departement;
    }
    if (i == 2) {
      this.departementService
        .supprimer(departement.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.departementList.findIndex((c) => c.id == departement.id);
            if (i > -1) {
              this.departementList.splice(i, 1);
              this.departementList = [...this.departementList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.departement = null;
    }
  }

  list(): void {
    this.loading = true;
    this.departementService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.departementList = data.object != null ? data.object : [];
        this.departementList = [...this.departementList];
        console.log(this.departementList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  submitForm(): void {
    for (const i in this.departementForm.controls) {
      this.departementForm.controls[i].markAsDirty();
      this.departementForm.controls[i].updateValueAndValidity();
    }

    if (this.departementForm.valid === true) {
      const formData = this.departementForm.value;
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.departementService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.departementList.findIndex((c) => c.id == formData.id);
              this.departementList[i] = data.object;
              this.departementList = [...this.departementList];
              this.departementForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.departementService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.departementList.unshift(data.object);
              this.departementList = [...this.departementList];
              this.departementForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

}
