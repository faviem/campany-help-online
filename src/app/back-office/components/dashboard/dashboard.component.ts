import {Component, OnInit, ViewChild} from '@angular/core';
import {Utilisateur} from "../../../models/auth/utilisateur";
import {Router} from "@angular/router";
import {TokenStorage} from "../../../utilitaires/token-storage";
import {StatistiqueService} from "../../../services/back-office/statistique.service";
import {ResponseBody} from "../../../models/response-body";
import {HttpErrorResponse} from "@angular/common/http";
import {Question} from "../../../models/back-office/question";
import {  ChartComponent } from "ng-apexcharts";

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ApexTheme,
  ApexTitleSubtitle,
  ApexFill,
  ApexStroke,
  ApexYAxis,
  ApexLegend,
  ApexPlotOptions
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  theme: ApexTheme;
  title: ApexTitleSubtitle;
  fill: ApexFill,
  yaxis: ApexYAxis,
  stroke: ApexStroke,
  legend: ApexLegend,
  plotOptions: ApexPlotOptions
};

export type ChartOptionsChart2 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
};

export interface Statistique {
  nombreQuestionsEnAttenteReponse: number;
  nombrePublicationGlobal: number;
  nombreQuestionGlobal: number;
  nombreQuestionParDepartement: number;
  nombreReponseGlobal: number;
  nombreReponseParDepartement: number;
  nombrePublicationParCategorie: number;
  list10DerniereQuestionsEnAttente: Question[];
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user: Utilisateur = null;
  statitstiqueData: Statistique = null;
  statCategorie: any[];

  /* graphes */
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  @ViewChild("chart2") chart2: ChartComponent;
  public chartOptions2: Partial<ChartOptionsChart2>;

  constructor(
    private router: Router,
    private tokenStorage: TokenStorage,
    private statistiqueService: StatistiqueService,
  ) {
  }

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    // console.log(this.user);
    this.statistiqueService.getPublicationByCategorie().subscribe(
      (data: ResponseBody) => {
        console.log(data);
        this.statCategorie = data.object != null ? data.object : [];

        this.chartOptions2 = {
          series: [
            {
              name: "Publication",
              data: this.statCategorie.map((s) => s.nombre),
            }
          ],
          chart: {
            type: "bar",
            height: 350
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: "55%"
            }
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ["transparent"]
          },
          xaxis: {
            categories: this.statCategorie.map((s) => s.libelle),
          },
          yaxis: {
            title: {
              text: "Nombre de publication"
            }
          },
          fill: {
            opacity: 1
          },
          tooltip: {
            y: {
              formatter: function(val) {
                return "" + val + " publications";
              }
            }
          }
        };

      });
    this.statistiqueService.getByStatistiqueList(null).subscribe(
      (data: ResponseBody) => {
        this.statitstiqueData = data.object;
        console.log(this.statitstiqueData);

        if(this.statitstiqueData != null) {
          this.chartOptions = {
            series: [
              this.statitstiqueData.nombreQuestionParDepartement,
              this.statitstiqueData.nombreReponseParDepartement,
              this.statitstiqueData.nombreQuestionsEnAttenteReponse,
            ],
            chart: {
              width: 380,
              type: 'polarArea'
            },
            labels: ['Question', 'Réponse', 'En attente'],
            fill: {
              opacity: 1
            },
            stroke: {
              width: 1,
              colors: undefined
            },
            yaxis: {
              show: false
            },
            legend: {
              position: 'bottom'
            },
            plotOptions: {
              polarArea: {
                rings: {
                  strokeWidth: 0
                }
              }
            },
            theme: {
              monochrome: {
                //    enabled: true,
                shadeTo: 'light',
                shadeIntensity: 0.6
              }
            }
          };
        }

      },
      (error: HttpErrorResponse) => {
      });

  }


}
