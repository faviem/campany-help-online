import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Table} from "primeng/table";
import {ResponseBody} from "../../../models/response-body";
import Swal from "sweetalert2";
import {HttpErrorResponse} from "@angular/common/http";
import {LienUtile} from "../../../models/back-office/lien-utile";
import {LienUtileService} from "../../../services/back-office/lien-utile.service";

@Component({
  selector: 'app-lien-utile',
  templateUrl: './lien-utile.component.html',
  styleUrls: ['./lien-utile.component.css']
})
export class LienUtileComponent implements OnInit {

  lienUtileForm: FormGroup;
  lienUtileList: LienUtile[] = [];
  displayedForm: boolean = false;
  lienUtile: LienUtile = null;
  loading: boolean = false;
  isEnregistrement: boolean = false;

  @ViewChild("dt", {static: false}) table: Table;

  constructor(
    private fb: FormBuilder,
    private lienUtileService: LienUtileService
  ) {
  }

  ngOnInit(): void {
    this.makeForm(null);
    this.list();
  }

  makeForm(lienUtile: LienUtile): void {
    this.lienUtileForm = this.fb.group({
      id: [lienUtile != null ? lienUtile.id : null],
      titre: [lienUtile != null ? lienUtile.titre : null,
        [Validators.required]],
      lien: [lienUtile != null ? lienUtile.lien : null,
        [Validators.required]],
      publie: [lienUtile != null ? lienUtile.publie : false],
    });
  }

  deleteLienUtileOnList(i: number, lienUtile: LienUtile): void {
    this.makeForm(null);
    if (i == 1) {
      this.lienUtile = lienUtile;
    }
    if (i == 2) {
      this.lienUtileService
        .supprimer(lienUtile.id)
        .subscribe((data: ResponseBody) => {
          console.log(data);
          if (data.success == true) {
            const i = this.lienUtileList.findIndex((c) => c.id == lienUtile.id);
            if (i > -1) {
              this.lienUtileList.splice(i, 1);
              this.lienUtileList = [...this.lienUtileList];
            }
            Swal.fire(
              "Success!",
              data.message,
              "success"
            );
          } else {
            Swal.fire("Erreur", data.message, "error");
          }
        });
      this.lienUtile = null;
    }
  }

  list(): void {
    this.loading = true;
    this.lienUtileService.liste().subscribe(
      (data: ResponseBody) => {
        this.loading = false;
        this.lienUtileList = data.object != null ? data.object : [];
        this.lienUtileList = [...this.lienUtileList];
        console.log(this.lienUtileList);
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
      });
  }

  submitForm(): void {
    for (const i in this.lienUtileForm.controls) {
      this.lienUtileForm.controls[i].markAsDirty();
      this.lienUtileForm.controls[i].updateValueAndValidity();
    }

    if (this.lienUtileForm.valid === true) {
      const formData = this.lienUtileForm.value;
      this.isEnregistrement = true;
      if (formData.id != null) {
        this.lienUtileService.mettreAjour(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              const i = this.lienUtileList.findIndex((c) => c.id == formData.id);
              this.lienUtileList[i] = data.object;
              this.lienUtileList = [...this.lienUtileList];
              this.lienUtileForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      } else {
        this.lienUtileService.enregistrer(formData).subscribe((data: ResponseBody) => {
            console.log(data);
            if (data.success == false) {
              Swal.fire("Erreur", data.message, "error");
            } else {
              this.lienUtileList.unshift(data.object);
              this.lienUtileList = [...this.lienUtileList];
              this.lienUtileForm.reset();
              this.makeForm(null);
              Swal.fire("Information", data.message, "info");
            }
            this.isEnregistrement = false;
          },
          (error: HttpErrorResponse) => {
            this.isEnregistrement = false;
          });
      }
    } else {
      Swal.fire("Erreur", "Formulaire invalide.", "error");
    }
  }

}
