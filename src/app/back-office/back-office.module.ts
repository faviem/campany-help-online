import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BackOfficeRoutingModule} from './back-office-routing.module';
import { BackOfficeComponent } from './back-office.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CategorieComponent } from './components/categorie/categorie.component';
import { MediasComponent } from './components/medias/medias.component';
import { MotCleComponent } from './components/mot-cle/mot-cle.component';
import { QuestionComponent } from './components/question/question.component';
import { ReponseComponent } from './components/reponse/reponse.component';
import {TableModule} from "primeng/table";
import { FooterComponent } from './footer/footer.component';
import { ComponentsComponent } from './components/components.component';
import { DepartementComponent } from './components/departement/departement.component';
import { PublicationComponent } from './components/publication/publication.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {UtilisateurComponent} from "./components/utilisateur/utilisateur.component";
import {DataViewModule} from "primeng/dataview";
import {EditorModule} from 'primeng/editor';
import {NgSelectModule} from "@ng-select/ng-select";
import {TabViewModule} from 'primeng/tabview';
import {PaginatorModule} from 'primeng/paginator';
import { InfoPublicationComponent } from './components/publication/info-publication/info-publication.component';
import { ReinitialyzePasswordComponent } from './components/current-user/reinitialyze-password/reinitialyze-password.component';
import { MonCompteComponent } from './components/current-user/mon-compte/mon-compte.component';
import { MesQuestionsComponent } from './components/current-user/mes-questions/mes-questions.component';
import { MesExperiencesComponent } from './components/current-user/mes-experiences/mes-experiences.component';
import { RepondreComponent } from './components/question/repondre/repondre.component';
import { ArchiveQuestionComponent } from './components/question/archive-question/archive-question.component';
import {NgApexchartsModule} from "ng-apexcharts";
import { DemandeCompteComponent } from './components/utilisateur/demande-compte/demande-compte.component';
import { InfoYoutubeComponent } from './components/info-youtube/info-youtube.component';
import { LienUtileComponent } from './components/lien-utile/lien-utile.component';

@NgModule({
  declarations: [
    BackOfficeComponent,
    CategorieComponent,
    MediasComponent,
    MotCleComponent,
    QuestionComponent,
    ReponseComponent,
    UtilisateurComponent,
    FooterComponent,
    ComponentsComponent,
    DepartementComponent,
    PublicationComponent,
    DashboardComponent,
    InfoPublicationComponent,
    ReinitialyzePasswordComponent,
    MonCompteComponent,
    MesQuestionsComponent,
    MesExperiencesComponent,
    RepondreComponent,
    ArchiveQuestionComponent,
    DemandeCompteComponent,
    InfoYoutubeComponent,
    LienUtileComponent,

  ],
  imports: [
    CommonModule,
    BackOfficeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DataViewModule,
    EditorModule,
    NgSelectModule,
    TabViewModule,
    PaginatorModule,

    NgApexchartsModule,
 ]
})
export class BackOfficeModule { }
