import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BackOfficeComponent} from './back-office.component';
import {CategorieComponent} from "./components/categorie/categorie.component";
import {DepartementComponent} from "./components/departement/departement.component";
import {QuestionComponent} from "./components/question/question.component";
import {ReponseComponent} from "./components/reponse/reponse.component";
import {MediasComponent} from "./components/medias/medias.component";
import {PublicationComponent} from "./components/publication/publication.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {UtilisateurComponent} from "./components/utilisateur/utilisateur.component";
import {MotCleComponent} from "./components/mot-cle/mot-cle.component";
import {MonCompteComponent} from "./components/current-user/mon-compte/mon-compte.component";
import {ReinitialyzePasswordComponent} from "./components/current-user/reinitialyze-password/reinitialyze-password.component";
import {MesExperiencesComponent} from "./components/current-user/mes-experiences/mes-experiences.component";
import {MesQuestionsComponent} from "./components/current-user/mes-questions/mes-questions.component";
import {ArchiveQuestionComponent} from "./components/question/archive-question/archive-question.component";
import {InfoYoutube} from "../models/back-office/info-youtube";
import {InfoYoutubeComponent} from "./components/info-youtube/info-youtube.component";
import {LienUtileComponent} from "./components/lien-utile/lien-utile.component";

const routes: Routes = [
  {
    path: '', component: BackOfficeComponent,
    children: [
      {
        path: '', component: DashboardComponent,
      },
      {
        path: 'departement', component: DepartementComponent,
      },
      {
        path: 'categorie', component: CategorieComponent,
      },
      {
        path: 'lien-youtube', component: InfoYoutubeComponent,
      },
      {
        path: 'lien-utile', component: LienUtileComponent,
      },
      {
        path: 'question', component: QuestionComponent,
      },
      {
        path: 'archive-question', component: ArchiveQuestionComponent,
      },
      {
        path: 'reponse', component: ReponseComponent,
      },
      {
        path: 'medias', component: MediasComponent,
      },
      {
        path: 'publication', component: PublicationComponent,
      },
      {
        path: 'nuage-mots', component: MotCleComponent,
      },
      {
        path: 'utilisateur', component: UtilisateurComponent,
      },
      {
        path: 'mon-profile', component: MonCompteComponent,
      },
      {
        path: 'reinitialisation', component: ReinitialyzePasswordComponent,
      },
      {
        path: 'mes-experiences', component: MesExperiencesComponent,
      },
      {
        path: 'mes-questions', component: MesQuestionsComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackOfficeRoutingModule { }
