import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string = environment.backend + '/auth';

  constructor(
      private http: HttpClient
  ) { }

  login(loginForm: any): Observable<Object> {
    return this.http.post(`${this.url}/authentifier-utilisateur`, loginForm);
  }

  reinitialyzeAccount(email: string): Observable<Object> {
    return this.http.post(`${this.url}/check-email-for-initialyze-compte`, email);
  }

}
