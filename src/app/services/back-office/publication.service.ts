import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Publication} from "../../models/back-office/publication";
import {PublicationSearch} from "../../models/back-office/publication-search";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  url: string = environment.backend + '/publication';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(publication: Publication): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, publication);
  }

  mettreAjour(publication: Publication): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, publication);
  }

  liste(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste?page=${page}&size=${size}`);
  }

  rechercher(page: number, size: number, rechercheRequest: PublicationSearch): Observable<Object> {
    return this.http.post(`${this.url}/recherche-publication?page=${page}&size=${size}`, rechercheRequest);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  getListeByCurrentUser(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-de-lutilisateur-connecte?page=${page}&size=${size}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
