import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Question} from "../../models/back-office/question";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  url: string = environment.backend + '/question';

  constructor(
      private http: HttpClient
  ) { }

  affecter(dataRequest: any): Observable<Object> {
    return this.http.post(`${this.url}/affecter`, dataRequest);
  }

  enregistrer(question: Question): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, question);
  }

  mettreAjour(question: Question): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, question);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
