import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {MotCle} from "../../models/back-office/mot-cle";

@Injectable({
  providedIn: 'root'
})
export class MotCleService {

  url: string = environment.backend + '/mot-cle';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(motCle: MotCle): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, motCle);
  }

  mettreAjour(motCle: MotCle): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, motCle);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
