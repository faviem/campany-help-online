import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Media} from "../../models/back-office/media";
import {filter} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  url: string = environment.backend + '/media';

  constructor(
      private http: HttpClient
  ) { }


  enregistrer(file: File): Observable<Object> {
    //if(documentSineb.id == null || documentSineb.id == 0){
    const formData = new FormData();
    formData.append('Content-Type', 'multipart/form-data');
    formData.append('file', file as File);
    const req = new HttpRequest('POST', this.url + '/enregistrer/', formData, {});
    return  this.http.request(req).pipe(filter(e => e instanceof HttpResponse));
  }

  mettreAjour(media: Media): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, media);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.delete(`${this.url}/supprimer/id/${id}`);
  }

  telecharger(fileName: string): Observable<any> {
    return this.http.get(`${this.url}/telecharger/${fileName}`, {responseType: "blob"});
  }


}
