import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Utilisateur} from "../../models/auth/utilisateur";

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  url: string = environment.backend + '/utilisateur';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(utilisateur: Utilisateur): Observable<Object> {
    return this.http.post(`${this.url}/creer-utilisateur`, utilisateur);
  }


  inscription(singupData: any): Observable<Object> {
    return this.http.post(`${this.url}/inscription`, singupData);
  }

  forgetPassword(forgetData: any): Observable<Object> {
    return this.http.post(`${this.url}/mot-de-passe-oublie`, forgetData);
  }

  mettreAjour(utilisateur: Utilisateur): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour-utilisateur`, utilisateur);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste-utilisateur`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/utilisateur/id/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

  verifierEmailDisponible(email: string): Observable<Object> {
    return this.http.get(`${this.url}/verifier-disponibilite-email?email=${email}`);
  }

  verifierUsernameDisponible(username: string): Observable<Object> {
    return this.http.get(`${this.url}/verifier-disponibilite-username?username=${username}`);
  }

  reinitialiserPassword(newPasswordForm: any): Observable<Object> {
    return this.http.post(`${this.url}/reinitialiser-mot-de-passe`, newPasswordForm);
  }

  /* les ressources de question */
  listeQuestionForDepartementOfCurrentUser(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-question-par-departement?page=${page}&size=${size}`);
  }

  listeQuestionForDepartementOfCurrentUserSansReponse(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-question-par-departement-sans-reponse?page=${page}&size=${size}`);
  }

  listeQuestionForUserByIdUser(id: number, page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-question-utilisateur-id?id=${id}&page=${page}&size=${size}`);
  }

  listeQuestionForUser(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-question-utilisateur-connecte?page=${page}&size=${size}`);
  }

  listePublicationOfCurrentUser(page: number, size: number): Observable<Object> {
    return this.http.get(`${this.url}/liste-de-lutilisateur-connecte?page=${page}&size=${size}`);
  }
  /* fin des ressources */


}
