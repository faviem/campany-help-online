import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {filter} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {

  url: string = environment.backend + '/statistique';

  constructor(
      private http: HttpClient
  ) { }

  listeRoles(): Observable<Object> {
    return this.http.get(`${this.url}/enums-role`);
  }

  getPublicationByCategorie(): Observable<Object> {
    return this.http.get(`${this.url}/publication-par-categorie`);
  }

  getByStatistiqueList(categorieId: number): Observable<Object> {
    if(categorieId != null && categorieId > 0) {
      return this.http.get(`${this.url}/all?categorieId=${categorieId}`);
    } else {
      return this.http.get(`${this.url}/all`);
    }
  }

}
