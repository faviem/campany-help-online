import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {filter} from "rxjs/operators";
import {LienUtile} from "../../models/back-office/lien-utile";

@Injectable({
  providedIn: 'root'
})
export class LienUtileService {

  url: string = environment.backend + '/lien-utile';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(lienUtile: LienUtile): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, lienUtile);
  }

  mettreAjour(lienUtile: LienUtile): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, lienUtile);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
