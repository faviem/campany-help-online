import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Departement} from "../../models/back-office/departement";

@Injectable({
  providedIn: 'root'
})
export class DepartementService {

  url: string = environment.backend + '/departement';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(departement: Departement): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, departement);
  }

  mettreAjour(departement: Departement): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, departement);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
