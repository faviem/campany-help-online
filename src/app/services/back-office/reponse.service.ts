import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Reponse} from "../../models/back-office/reponse";

@Injectable({
  providedIn: 'root'
})
export class ReponseService {

  url: string = environment.backend + '/reponse';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(reponse: any): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, reponse);
  }

  mettreAjour(reponse: Reponse): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, reponse);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  getListeByQuestion(idQuestion: number): Observable<Object> {
    return this.http.get(`${this.url}/liste/question/${idQuestion}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
