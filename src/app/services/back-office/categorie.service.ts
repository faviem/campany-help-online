import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {Categorie} from "../../models/back-office/categorie";

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  url: string = environment.backend + '/categorie';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(categorie: Categorie): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, categorie);
  }

  mettreAjour(categorie: Categorie): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, categorie);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
