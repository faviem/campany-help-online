import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {filter} from "rxjs/operators";
import {InfoYoutube} from "../../models/back-office/info-youtube";

@Injectable({
  providedIn: 'root'
})
export class InfoYoutubeService {

  url: string = environment.backend + '/info-youtube';

  constructor(
      private http: HttpClient
  ) { }

  enregistrer(infoYoutube: InfoYoutube): Observable<Object> {
    return this.http.post(`${this.url}/enregistrer`, infoYoutube);
  }

  mettreAjour(infoYoutube: InfoYoutube): Observable<Object> {
    return this.http.post(`${this.url}/mettre-a-jour`, infoYoutube);
  }

  liste(): Observable<Object> {
    return this.http.get(`${this.url}/liste`);
  }

  getById(id: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${id}`);
  }

  getListeByValeur(valeur: string): Observable<Object> {
    return this.http.get(`${this.url}/liste/${valeur}`);
  }

  supprimer(id: number): Observable<Object> {
    return this.http.get(`${this.url}/supprimer/${id}`);
  }

}
