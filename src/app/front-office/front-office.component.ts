import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {TokenStorage} from "../utilitaires/token-storage";
import {Utilisateur} from "../models/auth/utilisateur";

@Component({
  selector: 'app-front-office',
  templateUrl: './front-office.component.html',
  styleUrls: ['./front-office.component.css']
})
export class FrontOfficeComponent implements OnInit {

  user: Utilisateur = null;

  constructor(
    private router: Router,
    private tokenStorage: TokenStorage,
  ) { }

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    console.log(this.user);
  }

}
