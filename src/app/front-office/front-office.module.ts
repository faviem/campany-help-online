import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FrontOfficeRoutingModule} from './front-office-routing.module';
import {FrontOfficeComponent} from "./front-office.component";
import {HomeComponent} from "./home/home.component";

@NgModule({
  declarations: [
    FrontOfficeComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    FrontOfficeRoutingModule
  ]
})
export class FrontOfficeModule { }
