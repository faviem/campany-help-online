import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BackOfficeComponent} from "../back-office/back-office.component";
import {FrontOfficeComponent} from "./front-office.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {
    path: '', component: FrontOfficeComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'auth',
        loadChildren: () => import('../auth/auth.module').then(value => value.AuthModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontOfficeRoutingModule { }
